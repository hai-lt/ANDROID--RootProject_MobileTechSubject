# Root  Project for mobile tech subject
Place contains all sub-project (each project as a excersise) which we need to do at class or at home
- First project: Simple calculator https://gitlab.com/hai-lt/simple-calculator
- Prob 3.1 - work in class: https://gitlab.com/hai-lt/IntentDemo/tree/20170118-bai-tap-o-lop-Thiet-ke-giao-dien-login
- Prob 3.2 (homework) - DemoIntent: https://gitlab.com/hai-lt/IntentDemo/tree/master
- Prob 4.2 (homework) - https://gitlab.com/hai-lt/ANDROID--RootProject_MobileTechSubject/tree/project/20170207-prob4-homework/
    - Demo Photos: https://goo.gl/xVvBHH
- Prob 5.1 Coding with SQLite - https://gitlab.com/hai-lt/ANDROID--RootProject_MobileTechSubject/tree/project/20170211-prob-5-sql-exercise-at-home
    - Demo Photos: https://goo.gl/m4kfvK
- Prob 6.2 Coding with frament - https://gitlab.com/hai-lt/ANDROID--RootProject_MobileTechSubject/tree/project/20170219-prob-6-fragment-exercise
    - Demo photos: https://goo.gl/zPlle8