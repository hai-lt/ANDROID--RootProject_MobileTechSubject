package com.example.tanhai.rootproject.activities;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by tanhai on 20/02/2017.
 * {@link BaseActivity} is class that you need to extend when create an activity in this project.
 * It is an {@link AppCompatActivity}'s subclass.
 */

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView();
        setData();
        setUI();
        setEvent();
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView();
        setData();
        setUI();
        setEvent();
    }

    /**
     * set content view for activity
     */
    public abstract void setContentView();

    /**
     * initialize all the attributes and get data which is used for this activity
     */
    public abstract void setData();

    /**
     * initialize all views which is needed to use in this activity
     */
    public abstract void setUI();

    /**
     * add events for all views which is needed to use in this class
     */
    public abstract void setEvent();
}
